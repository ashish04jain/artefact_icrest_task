//
//  SearchViewController.swift
//  Artefact
//
//  Created by Ashish on 16/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit
import ObjectMapper

class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchField: UITextField!
    
    var searchTerm = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadNavBar()
        loadInitials()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    func loadInitials() {
        searchField.delegate = self
        searchField.returnKeyType = .search
    }
    
    func loadNavBar() {
        self.navigationController?.loadAttributes(navBarTintColorDefault: #colorLiteral(red: 0.9647058824, green: 0.968627451, blue: 0.9725490196, alpha: 1))
        self.navigationItem.title = Controllers.navigationTitle.search
    }
    
    @IBAction func didTapSearch(_ sender: Any) {
        doSearch()
    }
    
    func doSearch() {
        if searchTerm.count != 0 {
            fetchArticles()
        } else {
            self.showSlideNotification(notificationText: "Search articles to proceed!")
        }
    }
    
    func fetchArticles() {
        if !ReachabilityManager.sharedInstance.isReachable() {
            showNetworkAlert()
            return
        }
        self.showProgress(message: nil)
        
        let encodedString = searchTerm.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        AlamofireAPIWrapper.sharedInstance.searchArticles(searchTerm: encodedString!, page: 0) { (response) in
            let (success, result) = self.validateServerResponse(response: response, showErrorAlert: true)
            if success {
                if let res = result?["response"] as? JSONDictionary {
                    if let docs = res["docs"] as? JSONArray {
                        let articles = Mapper<Article>().mapArray(JSONObject: docs)!
                        if articles.count > 0 {
                            self.nextVC(articles: articles)
                        } else {
                            self.showSlideNotification(notificationText: "No article found for specified search!")
                        }
                    }
                }
            }
            self.hideProgress()
        }
    }
    
    func nextVC(articles: [Article]) {
        let vc = ArticlesViewController.instantiate(fromAppStoryboard: .Article)
        vc.articles = articles
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension SearchViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        doSearch()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            searchTerm = text.replacingCharacters(in: textRange,
                                                  with: string)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
}


@IBDesignable
class PaddedTextField: UITextField {
    
    @IBInspectable var inset: CGFloat = 0
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: inset, dy: inset)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
}


