//
//  HomeViewController.swift
//  Artefact
//
//  Created by Ashish on 16/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit
import ObjectMapper

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var popularTypes: [PopularType] = [.viewed, .shared, .emailed]
    var sectionKeys = ["Search", "Popular"]
    var sectionvalues = [["Search Articles"], [PopularType.viewed.title, PopularType.shared.title, PopularType.emailed.title]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadNavBar()
    }
    
    func loadNavBar() {
        self.navigationController?.loadAttributes(navBarTintColorDefault: #colorLiteral(red: 0.9647058824, green: 0.968627451, blue: 0.9725490196, alpha: 1))
        self.navigationItem.title = Controllers.navigationTitle.home
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
    func fetchArticles(popularType: String) {
        if !ReachabilityManager.sharedInstance.isReachable() {
            showNetworkAlert()
            return
        }
        
        self.showProgress(message: nil)
        AlamofireAPIWrapper.sharedInstance.fetchArticles(popularType: popularType) { (response) in
            let (success, result) = self.validateServerResponse(response: response, showErrorAlert: true)
            if success {
                if let resultRes = result?["results"] as? JSONArray {
                    let articles = Mapper<Article>().mapArray(JSONObject: resultRes)!
                    if articles.count > 0 {
                        self.showArticlesVC(articles: articles)
                    } else {
                        self.showSlideNotification(notificationText: "No article found!")
                    }
                }
            }
            self.hideProgress()
        }
    }
    
    func showArticlesVC(articles: [Article]) {
        let vc = ArticlesViewController.instantiate(fromAppStoryboard: .Article)
        vc.articles = articles
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionKeys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionvalues[section].count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionKeys[section]
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "cellId")
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        
        let value = sectionvalues[indexPath.section][indexPath.row]
        cell.textLabel?.text = value
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let vc = SearchViewController.instantiate(fromAppStoryboard: .Article)
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            fetchArticles(popularType: popularTypes[indexPath.row].rawValue)
        }
    }
    
}
