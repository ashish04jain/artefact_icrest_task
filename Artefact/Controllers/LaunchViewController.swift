//
//  LaunchViewController.swift
//  Artefact
//
//  Created by Ashish on 16/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {
    
    let appSession = AppSession.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()
        appSession.loadHomeView()
    }

}

