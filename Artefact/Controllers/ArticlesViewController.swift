//
//  ArticlesViewController.swift
//  Artefact
//
//  Created by Ashish on 16/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit

class ArticlesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var articles = [Article]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadNavBar()
        loadInitials()
    }
    
    func loadNavBar() {
        self.navigationController?.loadAttributes(navBarTintColorDefault: #colorLiteral(red: 0.9647058824, green: 0.968627451, blue: 0.9725490196, alpha: 1))
        self.navigationItem.title = Controllers.navigationTitle.articles
    }
    
    func loadInitials() {
        tableView.separatorStyle = .none
        tableView.reloadData()
    }
    
}

extension ArticlesViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 1.0))
        view.backgroundColor = UIColor.gray
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: "cellId")
        cell.selectionStyle = .none
        
        let article = articles[indexPath.section]
        cell.textLabel?.text = article.title ?? ""
        cell.detailTextLabel?.text = article.publishedOn.toDateString()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
}

