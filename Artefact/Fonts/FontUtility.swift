
//
//  FontUtility.swift
//  Artefact
//
//  Created by Ashish on 16/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit

public func navigationTitleFont(fontName: String = "Avenir Book",size: CGFloat = 28) -> UIFont{
    return UIFont(name: fontName, size: size)!
}

public func navigationBarButtonTitleFont() -> UIFont{
    return UIFont(name: "Avenir Book", size: 16)!
}

public func navigationRightBarButtonTitleFont() -> UIFont {
    return UIFont(name: "Avenir Book", size: 16)!
}

public func placeHolderFont(fontSize: CGFloat = 16.0) -> UIFont{
    return UIFont(name: "Avenir Book", size: fontSize)!
}

public func defaultFont(size: CGFloat, fontName: String = "Avenir Book") -> UIFont {
    return UIFont(name: fontName, size: size)!
}
