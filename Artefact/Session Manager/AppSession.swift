//
//  AppSession.swift
//  Artefact
//
//  Created by Ashish on 16/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit

class AppSession: NSObject {
    
    static let sharedInstance = AppSession()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var homeNavVC: UINavigationController?
    var homeVC: HomeViewController?
    
    override init() {
        super.init()
    }

    func loadHomeView() {
        if homeNavVC == nil {
            homeVC = HomeViewController.instantiate(fromAppStoryboard: .Main)
            homeNavVC = UINavigationController(rootViewController: homeVC!)
        }
        
        let window = appDelegate.window!
        window.rootViewController = homeNavVC
    }
    
}
