//
//  AlamofireAPIResponse.swift
//  Artefact
//
//  Created by Ashish on 16/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit

class AlamofireAPIResponse: NSObject {
    
    let errorMsg: String?
    let errorCode: Int
    let responseObject: JSON?
    let isSuccessful: Bool
    
    init(response: JSON?, errorCode: Int, errorMessage: String, successful: Bool) {
        
        self.responseObject = response
        self.errorCode = errorCode
        self.errorMsg = errorMessage
        self.isSuccessful = successful
    }
}
