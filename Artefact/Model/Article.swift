//
//  Article.swift
//  Artefact
//
//  Created by Ashish on 16/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import Foundation
import ObjectMapper

class Article: NSObject, Mappable {
    
    var id: String?
    var title: String?
    var publishedOn: Date!
    
    override init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["_id"]
        if id == nil {
            id <- map["id"]
        }
        title <- map["headline.main"]
        if title == nil {
            title <- map["title"]
        }

        if let dateString = map["pub_date"].currentValue as? String {
            publishedOn = getDateFromTZString(string: dateString)
        } else if let dateString = map["published_date"].currentValue as? String {
            publishedOn = getDateFromString(string: dateString)
        }
    }
}


