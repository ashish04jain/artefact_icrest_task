
//
//  AlamofireAPIWrapper.swift
//  Artefact
//
//  Created by Ashish on 16/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit
import Alamofire

let baseURL = "https://api.nytimes.com/svc/"
let versionURL = "v2"

class AlamofireAPIWrapper: NSObject {
    
    static let sharedInstance = AlamofireAPIWrapper()

    var headers: [String: String] {
        get {
            return ["Content-Type": "application/json", "Accept": "application/json"]
        }
    }
    
    func searchArticles(searchTerm: String, page: Int, responseBlock: @escaping CompletionBlock) {
        let urlString = "search/\(versionURL)/articlesearch.json?q=\(searchTerm)&page=\(page)&sort=newest&api-key=\(Keys.nytAPIKey)"
        Alamofire.request(baseURL + urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: {(response) -> Void in
            if let error = response.result.error {
                print(error.localizedDescription)
                let apiResponse = AlamofireAPIResponse.init(response: nil, errorCode: (error as NSError).code, errorMessage: error.localizedDescription, successful: false)
                responseBlock(apiResponse)
            } else if let jsonValue = response.result.value {
                print(jsonValue)
                let apiResponse = AlamofireAPIResponse.init(response: jsonValue as JSON?, errorCode: 0, errorMessage: "", successful: true)
                responseBlock(apiResponse)
            }
        })
    }
    
    func fetchArticles(popularType: String, responseBlock: @escaping CompletionBlock) {
        let urlString = "mostpopular/\(versionURL)/\(popularType)/\(30).json?api-key=\(Keys.nytAPIKey)"
        Alamofire.request(baseURL + urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: {(response) -> Void in
            if let error = response.result.error {
                print(error.localizedDescription)
                let apiResponse = AlamofireAPIResponse.init(response: nil, errorCode: (error as NSError).code, errorMessage: error.localizedDescription, successful: false)
                responseBlock(apiResponse)
            } else if let jsonValue = response.result.value {
                print(jsonValue)
                let apiResponse = AlamofireAPIResponse.init(response: jsonValue as JSON?, errorCode: 0, errorMessage: "", successful: true)
                responseBlock(apiResponse)
            }
        })
    }
    
}
