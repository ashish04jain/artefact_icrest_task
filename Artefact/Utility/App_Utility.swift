//
//  App_Utility.swift
//  Artefact
//
//  Created by Ashish on 16/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit

func addCustomNavigationBar(viewController:UIViewController,height:CGFloat, yPosition: CGFloat) -> CustomNavigationBar{
    let customNavigation = CustomNavigationBar(frame: CGRect(x: 0, y: yPosition, width: viewController.view.frame.width, height: height) , target: viewController)
    viewController.view.addSubview(customNavigation)
    return customNavigation
}

func getDateFromTZString(string:String) -> Date{
    //2017-12-20T18:04:08+0000
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
    let date = dateFormatter.date(from: string)
    
    return date!
}

func getDateFromString(string:String) -> Date{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    let date = dateFormatter.date(from: string)
    
    return date!
}

extension UILabel {
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0, alignment: NSTextAlignment) {
        
        guard let labelText = self.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        paragraphStyle.alignment = alignment
        
        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        
        // Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
}

