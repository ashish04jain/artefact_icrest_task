//
//  Constants.swift
//  Artefact
//
//  Created by Ashish on 16/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import Foundation
import UIKit

typealias CompletionBlock = (AlamofireAPIResponse) -> Void
typealias JSON = AnyObject
typealias JSONDictionary = Dictionary<String, JSON>
typealias JSONArray = Array<JSON>

struct Controllers {

    struct navigationTitle {
        static let home = "NYT"
        static let search = "Search"
        static let articles = "Articles"
    }
}

// MARK: APIKeys
struct Keys {

    static let nytAPIKey = "VEAZqWLDtdfcssMMRjDTsJKQx1L0qBnN"
    static let nytAPISecret = "gmPvdnDzygcW67Ss"
}

enum PopularType: String {
    case viewed, shared, emailed
    
    var title: String {
        get {
            switch self {
            case .viewed:
                return "Most Viewed"
            case .shared:
                return "Most Shared"
            case .emailed:
                return "Most Emailed"
            }
        }
    }
}

// MARK :General Constants
struct AppConstants {
    
    struct Error {
        static let networkMsg = "Please check your internet connection and try again!"
        static let generalMsg = "We are notified and working on it, we will be back soon! Please try again later"
    }
    
    static let offWhiteBGOpacity = 0.4
    
    static let cornerRadius = CGFloat(10.0)
}

//MARK:Placeholder Images
struct PlaceholderImages {
    static let profile = "profile_holder_icon"
}

//MARK:Server Response Key
struct ServerResponseKey {
    static let SUCCESS = "status"
    static let ERROR = "error"
}

